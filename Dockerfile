FROM alpine:latest

RUN apk update && apk add curl
COPY ./run.sh /.

CMD ./run.sh